

class Temperature
  def initialize(hash = {})
    @hash = hash
    @fahrenheit = @hash[:f]
    @celcius = @hash[:c]
  end

  def self.from_celsius(c)
    Temperature.new(c: c)
  end

  def self.from_fahrenheit(f)
    Temperature.new(f: f)
  end

  def in_fahrenheit
    @fahrenheit || (@celcius * 9.0 / 5 + 32)
  end

  def in_celsius
    @celcius || ((@fahrenheit - 32) * 5.0 / 9)
  end

end

class Celsius < Temperature
  def initialize(temp)
    @celcius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
