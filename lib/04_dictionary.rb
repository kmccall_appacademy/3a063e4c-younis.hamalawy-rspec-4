class Dictionary
  def initialize
    @dic = {}
  end

  def entries
    @dic
  end

  def add(word)
    if word.class == String
      @dic[word] = nil
    else
      @dic[word.keys.first] = word.values.first
    end
  end

  def keywords
    @dic.keys.sort
  end

  def include?(word)
    @dic.keys.include?(word)
  end

  def find(word)
    result = {}
    @dic.keys.each do |key|
      result[key] = @dic[key] if key.include?(word)
    end
    result
  end

  def printable
    result = ""
    self.keywords.each do |key|
      if result.empty?
        result << "[#{key}] \"#{@dic[key]}\""
      else
        result << "\n[#{key}] \"#{@dic[key]}\""
      end
    end
    result
  end

end
