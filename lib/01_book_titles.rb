class Book < String
   # TODO: your code goes here!

  def title=(title)
      @title = title
  end

  def title
    titled_str = []
    exceptions = %w( a aboard about above absent across after against along alongside amid amidst among amongst an and around as aslant astride at athwart atop barring before behind below beneath beside besides between beyond but by despite down during except for from in inside into mid minus near next nor notwithstanding of off on onto opposite or out outside over past per plus regarding round since so than the through throughout till to toward towards under underneath until up upon via vs. with within without yet )
    str = @title.split
    str.each_index do |idx|
      if idx == 0
        titled_str << str[idx].capitalize
      elsif exceptions.include?(str[idx])
        titled_str << str[idx]
      else
        titled_str << str[idx].capitalize
      end
    end
    titled_str.join(' ')
  end
end
