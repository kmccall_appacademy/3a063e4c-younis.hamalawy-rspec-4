class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    min = (seconds / 60)
    hr = (min / 60)
    seconds = @seconds % 60
    min = min % 60
    min = min.to_s.rjust(2, '0')
    seconds = seconds.to_s.rjust(2, '0')
    hr = hr.to_s.rjust(2, '0')
    "#{hr}:#{min}:#{seconds}"
  end
end
